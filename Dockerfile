FROM node:14.16.1-alpine3.13

LABEL maintainer="gabrielarellano@gmail.com"

ENV JQ_VERSION=1.6-r1

# Install JQ
RUN apk add --no-cache jq=$JQ_VERSION


